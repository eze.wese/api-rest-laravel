<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use function GuzzleHttp\Psr7\try_fopen;
use Illuminate\Support\Facades\DB;
use App\User;

class JwtAuth {

		public $key;

		public function __construct(){
				$this->key = '123456789.key';
		}

	public function signup($email, $password, $getToken = null) {

        //Buscar usuario existente con credencial, por email y contraseña
				$user = User::where([
					'email' => $email,
					'password' => $password
				])->first();

        //Comprobacion si devuelve un OBJETO
				$signup = false;
				if(is_object($user)){
					$signup = true;
				}

        //Generacion del token con los datos del usuario identificado
				if($signup){

					$token = array(
						'sub' => $user->id,
						'email' => $user->email,
						'name' => $user->name,
						'surname' => $user->surname,
						'iat' => time(),
						'exp' => time() + (7 * 24 * 60 * 60)
					);

					$jwt = JWT::encode($token, $this->key, 'HS256');
					$decode = JWT::decode($jwt, $this->key, ['HS256']);

					//Devolver datos decodificados o token, en funcion de un paramentro
					if(is_null($getToken)){
						$data = $jwt;
					}
					else{
						$data = $decode;
					}

				}
				else{
					$data = array(
						'status' => 'error',
						'message' => 'Login incorrecto'
					);
				}



        return $data;
	}

	public function checkToken($jwt, $getIdentity = false){

			$auth = false;

			try {
			    $jwt = str_replace('"', '', $jwt);
				$decode = JWT::decode($jwt, $this->key, ['HS256']);
			}catch (\UnexpectedValueException $e){
				$auth = false;
			}catch (\DomainException $e){
				$auth = false;
			}

			if(!empty($decode) && is_object($decode) && isset($decode->sub)){
                $auth = true;
			}else{
			    $auth = false;
            }

			if($getIdentity){
			    return $decode;
            }

			return $auth;

	}

}
